# HL7-OLM

HL7 OLM Messaging for OpenMRS-based Rails applications

## Setup

Add the settings below in your application's `config/application.yml`. Replace
`development` with your RAILS environment

    development:
      hl7.sending_app: "TESTAPP^2.16.840.1.114222.4.3.2.1..^ISO"
      hl7.sending_facility: "Test Center^2.16.840.1.114222.4.1.^ISO"
      hl7.recv_app: "^2.16.840.1.114222.4.3.2.3^ISO"
      hl7.recv_facility: "^2.16.840.1.114222.4.1.1^ISO"

Build your gem and then copy it to you application's `vendor/cache/` directory

    gem build hl7-olm.gemspec
    cp hl7-olm-X.X.X.gem <PATH-TO-RAILS-APP>/vendor/cache

Include `hl7-olm`, `ruby-hl7` and `rest-client` in your `Gemfile`

    gem 'ruby-hl7'
    gem 'rest-client'
    gem 'hl7-olm'

Install gems for your application

    cd <PATH-TO-RAILS-APP>
    bundle install
    
## Example
  
    require 'ruby-hl7'
    require 'rest-client'
    require 'open-uri'
    require 'hl7-olm'
    
    enc = Encounter.find(6)
    msg = MessagingService::HL7OLM.message(enc)
    
    puts msg.to_s # readable version of the message
    
    host_url = <LMIS_HOST> # '192.168.1.1:8080/openElis'
    post_action = <POST_ACTION> # e.g. 'rest/OML/submit'
    
    hl7_msg = msg.to_s
    post_params = {'username' => <USERNAME>, 'password' => <PASSWORD>, 'hl7' => hl7_msg}
    
    begin
      RestClient.log = "/tmp/hl7-olm.log"
      resp = RestClient.post("http://#{host_url}/#{post_action}", post_params)
      puts "------------------resp---------------------"
      puts resp.body
    rescue Exception => e
      puts e.message
    end

  