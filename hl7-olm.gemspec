# -*- encoding: utf-8 -*- 
$:.push File.expand_path("../lib", __FILE__) 
require "version"


Gem::Specification.new do |s|
  s.name        = %q{hl7-olm}
  s.version     = HL7OLM::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Soyapi Mumba"]
  s.email       = %q{soyapim@gmail.com}
  s.summary     = %q{Order HL7 Messaging Service}
  s.description = %q{HL7 v2.5 OLM Messaging}
  
  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
  
end
