
module MessagingService
  module HL7OLM
    
    def self.message(encounter)
      msg = HL7::Message.new
      msh = MessagingService::HL7OLM.msh
      msg << msh
      
      msg << MessagingService::HL7OLM.pid(encounter.patient.person)
      msg << MessagingService::HL7OLM.orc(msh, encounter)
      msg << MessagingService::HL7OLM.tq1(msh)
      msg << MessagingService::HL7OLM.obr
      msg << MessagingService::HL7OLM.spm
      msg << MessagingService::HL7OLM.sac
      
      msg
    end
    
    def self.msh
      msh = HL7::Message::Segment::MSH.new
      msh.enc_chars = "^~\\&"
      msh.sending_app = CoreService.get_global_property_value('hl7.sending_app')
      msh.sending_facility = CoreService.get_global_property_value('hl7.sending_facility')
      msh.recv_app = CoreService.get_global_property_value('hl7.rec_app')
      msh.recv_facility = CoreService.get_global_property_value('hl7.recv_facility')
      msh.time = Time.now.strftime("%Y%m%d%H%M%S")
      msh.message_type = "OML^O21^OMLO21"
      msh.message_control_id = msh.time
      msh.processing_id = "P^T"
      msh.version_id = 2.5
      msh.message_profile_identifier = "1.0\r"
      
      msh
    end
    
    def self.pid(person)
      patient = person.patient
      pid = HL7::Message::Segment::PID.new
      #pid.set_id = 1
      pid.patient_id_list = PatientService.get_patient_identifier(patient,
                                                    "Old Identification Number")
      pid.patient_name = full_name(person)
      pid.patient_dob = person.birthdate.strftime("%Y%m%d")
      pid.admin_sex = person.gender
      pid.patient_alias = "\r"
      
      pid
    end
    
    def self.orc(msh, encounter)
      orc = HL7::Message::Segment::ORC.new
      orc.order_control = 'NW'
      orc.placer_order_number = '908654^2.16.840.1.114222.4.3.2.1...3.5^ISO'

      orc.placer_group_number = '231^2.16.840.1.114222.4.3.2.1...3.5^ISO'
      orc.order_status = 'IP'
      orc.response_flag = 'F'
      orc.date_time_of_transaction = msh.time
      orc.entered_by = creator(User.find(encounter.creator))
      orc.verified_by = provider(encounter.provider)
      orc.ordering_provider = provider(encounter.provider)
      orc.order_effective_date_time = msh.time
      orc.ordering_facility_name = Location.current_health_center.name + "\r"

      orc
    end

    def self.tq1(msh)
      tq1 = HL7::Message::Segment::TQ1.new
      tq1.set_id = 1    
      tq1.quantity = 1
      tq1.repeat_pattern = "Once"
      tq1.end_datetime = msh.time
      tq1.condition_text = "S\r"
      
      tq1
    end
    
    def self.obr
      obr = HL7::Message::Segment::OBR.new
      # obr.set_id = 1
      obr.placer_order_number = "908654^2.16.840.1.114222.4.3.2.1...3.5^ISO"
      obr.universal_service_id = "626-2^MICROORGANISM IDENTIFIED:PRID:PT:THRT:NOM:THROAT CULTURE^LOINC^78335^Throat Culture^L"
      obr.specimen_source = "2.16.840.1.114222.4.1.213^Jones^M^J^Jr^Dr^MD"
      obr.ordering_provider = "^^^^^206^9998888\r"

      obr      
    end
    
    def self.spm
      spm = HL7::Message::Segment::SPM.new
      spm.set_id = 1
      spm.specimen_id = 38294522
      spm.specimen_type = "THRT^Throat\r"
      
      spm
    end
    
    def self.sac
      sac = HL7::Message::Segment::SAC.new
      sac.accession_identifier = "B96346"
      sac.primary_container_identifier = "1ZE80A71124372^UPS\r"
      
      sac
    end
    
private
    # e.g. Doe^John^Q^Jr
    def self.full_name(person)
      n = person.names.last
      
      "#{n.family_name}^#{n.given_name}^#{n.middle_name}"
    end
    
    # e.g. Jones^M^J^Jr^Dr^MD
    def self.provider_name(person)
      n = person.names.last
      
      "#{n.family_name}^#{n.given_name.first}^#{n.middle_name.first}" +
      "^#{n.family_name_suffix}^#{n.prefix}^^"
    end
    
    # e.g. 2.16.840.1.114222.4.1.212^Nurse^Nancy
    def self.creator(user)
      begin
        name = full_name(user.person)
        role = user.user_roles.first.role
      rescue Exception => e
        raise "Could not find user who entered this order: #{e.message}"
      end
      
      "2.16.840.1.114222.4.1.212^#{role}^#{name}"
    end
    
    # e.g. 2.16.840.1.114222.4.1.213^Jones^M^J^Jr^Dr^MD
    def self.provider(person)
      "2.16.840.1.114222.4.1.213^#{provider_name(person)}"
    end
  end
  
end