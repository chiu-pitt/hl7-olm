class HL7::Message::Segment::SAC < HL7::Message::Segment

  weight 1002 # fixme
#  has_children [:OBX]
  add_field :external_accession_identifier
  add_field :accession_identifier
  add_field :container_identifier
  add_field :primary_container_identifier
  add_field :equipment_container_identifier
  add_field :specimen_source
  add_field :registration_date_time
  add_field :container_status
  add_field :carrier_type
  add_field :carrier_identifier
  add_field :position_in_carrier
  add_field :tray_type_sac
  add_field :tray_identifier
  add_field :position_in_tray
  add_field :location
  add_field :container_height
  add_field :container_diameter
  add_field :barrier_delta
  add_field :bottom_delta
  add_field :container_height_diameter_delta_units
  add_field :container_volume
  add_field :available_specimen_volume
  add_field :initial_specimen_volume
  add_field :volume_units
  add_field :separator_type
  add_field :cap_type
  add_field :additive
  add_field :specimen_component
  add_field :dilution_factor
  add_field :treatment
  add_field :temperature
  add_field :hemolysis_index
  add_field :hemolysis_index_units
  add_field :lipemia_index
  add_field :lipemia_index_units
  add_field :icterus_index
  add_field :icterus_index_units
  add_field :fibrin_index
  add_field :fibrin_index_units
  add_field :system_induced_contaminants
  add_field :drug_interference
  add_field :artificial_blood
  add_field :special_handling_code
  add_field :other_environmental_factors
end
