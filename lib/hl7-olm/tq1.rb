class HL7::Message::Segment::TQ1 < HL7::Message::Segment

  weight 10001 # fixme
  add_field :set_id
  add_field :quantity
  add_field :repeat_pattern
  add_field :explicit_time
  add_field :relative_time_and_units
  add_field :service_duration
  add_field :start_datetime
  add_field :end_datetime
  add_field :priority
  add_field :condition_text
  add_field :text_instruction
  add_field :conjunction
  add_field :ocurrence
end